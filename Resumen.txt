Arrancar un proyecto ya creado
    1. Arrancar Xamp: Apache y mySQL
    ?? quizá para confirmar que la bbdd está conectada php bin/console doctrine:schema:update --force

Cómo crear un proyecto symfony

    1. Instalar Composerc

    2. Instalar Symfony. Carpeta xampp/htdocs:

        -composer create-project symfony/website-skeleton + nombre
        -comprobamos que está ejecutándose en: http://localhost/Bootcamp/website-skeleton/public/

    3. Crear BBBDD:

        -Configuro .env (si mysql, el usuario siempre root y sin password. El nombre de bbdd es el que puede cambiar)
        -DATABASE_URL="mysql://root@127.0.0.1:3306/bbdd?serverVersion=5.7"
        -Terminal: php bin/console doctrine:database:create (no funcionó en VScode terminal. Lo hice desde cmd)
        -Podemos comprobar que se creó en http://localhost/phpmyadmin/

    4. Crear tablas de BBDD/Crear en Symfony Entidades (son lo mismo):

        -Terminal: php bin/console make:entity >
            Crea una clase Entidad en Symfony que se corresponderá con una tabla de nuestra BBDD en mySQL. 
            Tenemos que definir propiedades de las Entidades, que se corresponderan con los campos de nuestra tabla. 
            Si se nos olvida alguna, o queremos añadir más propiedades/campos más adelante, basta con definirlas como variables en la entidad. No olvidar poner arriba los comentarios @OMR y crear sus get y set abajo

        -Terminal: php bin/console doctrine:schema:update --force > 
            busca en la carpeta Entity las entidades y las pasa a la BBDD. Si ya estaban, las actualiza.

        -(Hay Entidades especiales. En el caso de este ejemplo, una tabla que se usará para autenticar usuarios. Ejecutamos el comando: php bin/console make:user. Por defecto, la entidad user te permite elegir si se usará password o email para acceder)

        -(Si hacemos más de una tabla, debemos establecer relaciones entre ellas. Explicado en: https://www.youtube.com/watch?v=mErEwsHzD0M)
    
    5. Rutas: clase Controller

        -Crear un controlador > php bin/console make:controller >
             Se crea una clase en la carpeta Controller y una carpeta en twig que están vinculadas

    6. Usuarios: registro, login y formularios >

         -6.1. Registro Ususarios
            1. Hay que crear una clase especial User (ver punto 4, tercer guión, de este archivo) que es una clase controller para gestionar Ususarios
            2. Crear un controlador normal para gestionar el registro de Usuarios (RegistroController)

        -6.2. Crear un formulario de Registro> php bin/console make:form >
            1. Nombre del formulario: siempre acaban en Type (UserType)
            2. A qué entidad estará vinculado dicho formulario (User)
            3. Se crea una carpeta Form con un archivo de formulario (tipo especial de clase). Aparecerá que los campos del formulario se corresponden con las propiedades de la Entidad a la que se relacionó, que a su vez se relaciona con los campos de la tabla de base de datos
            4. En FormType tenemos que agregar el botón del formulario: ver UserType o DemoType > ->add('Enviar', SubmitType::class)
            4. Pasar el formulario a una template: ver RegistroController.php > function index() (puntos 1-3)
            5. Pasar datos de formulario a BBDD: ver RegistroController.php > function index() (puntos 4-11)

        -6.3. Encriptar contraseñas: UserPasswordEncoderInterface.
            -Indicamos arriba que vamos a usar la interfaz (RegistroController): use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
            -Le ponemos como argumento al constructor: UserPasswordEncoderInterface
            -Funciones de encriptado: ver RegistroController.php > function index() ((puntos 12))

        -6.4. Paquete de seguridad para login:
            1. Requisitos: tener una clase tipo User y tener la BBDD actualizada (php bin/console doctrine:schema:update --force ),
            2. El paquete security-bundle nos ayuda a configurarlo: composer require symfony/security-bundle. Deberíamos tener config > packages > security.yaml con ésto arriba: security: encoders: App\Entity\User: algorithm: auto
        
        -6.5. Crear un formulario de login:
            1. php bin/console make:auth > 1 > LoginFormAuthenticator > enter > enter. Se ha creado la carpeta Security y un nuevo Controller.
            2. Tenemos que indicar a qué página nos redirigirá cuando se autentique el usuario. En LoginFormAuthenticator, añadimos return new RedirectResponse('main'); en la línea 100, tras el comentario. Dashboard es lo que puede cambiar
            3. Desde /login podremos acceder al login de usuario y, si es válido, accederemos a dashboard
    
    7. Consultas a BBDD

        1. El objeto Entity Manager sirve para gestionar BBDD. Hasta ahora hemos visto cómo enviar datos.
        2. Desde el controller, en la función index: 
                    $em = $this->getDoctrine()->getManager();//llamamos a Entity Manager
                    $dbPost = $em->getRepository(PostsType::class)->findAll();//Guardamos en una variable la llamada al respositorio e indicamos qué queremos (all en este caso)
                    

