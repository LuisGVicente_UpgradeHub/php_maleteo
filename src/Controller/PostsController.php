<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Form\PostsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index(Request $request): Response
    {
        $posts = new Posts();
        $postsForm = $this->createForm(PostsType::class,$posts);
        $postsForm->handleRequest($request);
        if($postsForm->isSubmitted() && $postsForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($posts);
            $em->flush();
            $this->addFlash('exito','Gracias por dejarnos tu opinión');
            return $this->redirectToRoute('posts');
        }    
        $em = $this->getDoctrine()->getManager();
        $dbPost = $em->getRepository(Posts::class)->findAll();
        

            
        return $this->render('posts/index.html.twig', [
            'posts' => $postsForm->createView(),
            'dbPost' => $dbPost
        ],);
    }

    /**
     * @Route("/main", name="main")                      //NO FUNCIONA
     */
    // public function getPostDB ( EntityManagerInterface $doctrine) {
    //     $rePost = $doctrine->getRepository(Posts::class);
    //     $post = $rePost->findAll();
    //     return $this->render('main/index.html.twig', ['post' => $post]);
    // }    
}
