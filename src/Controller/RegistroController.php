<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistroController extends AbstractController
{
    /**
     * @Route("/registro", name="registro")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user= new User();//1. Creamos un objeto de la Entity que queremos vincular a nuestro formulario
        $form = $this->createForm(UserType::class,$user);//2. Con el método createForm, vinculamos el formulario con la Entity
        $form->handleRequest($request);//5. Nos permite saber si los datos del formulario fueron enviados
        if($form->isSubmitted() && $form->isValid()){//6. Qué hacer si el formulario fue enviado y de forma válida
            $em = $this->getDoctrine()->getManager();//7. Creamos un Entity Manager.Lo necesitamos para trabajar con una BBDD
            $user->setRoles(['ROLE USER']);
            $user->setPassword($passwordEncoder->encodePassword($user,$form['password']->getData()));//12.Necesario para encriptar el password
            $em->persist($user);//8.Le doy el atributo persist para que los datos persistan
            $em->flush();//9.Envía los datos a la BBDD
            $this->addFlash('exito','Se ha registrado correctamente');//10. Mensaje.Para que se muestre, la variable debe ir en un {% for message in app.flashes('exito')%}<div class="alert alert-success">{{message}}<div/>{% endfor %}
            
            return $this->redirectToRoute('main');//11. Permite redirigir a una página. Usamos el "name" de la @Route

        }

        return $this->render('registro/index.html.twig', [
            'controller_name' => 'RegistroController funcionando',
            'formulario' => $form->createView()
        ]);//3. A la variable que guarda la vinculación form-Entity, le aplicamos createView()
        //Vinculamos una variable cualquiera('formulario') para que podamos verla en una plantilla twig(registro/index.html.twig)
    }

}
