<?php

namespace App\Controller;

use App\Entity\Demo;
use App\Entity\Posts;
use App\Form\DemoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    
    /**
     * @Route("/main", name="main")
     */
    public function index(Request $request, EntityManagerInterface $doctrine): Response
    {
        $demo= new Demo();
        $demoForm = $this->createForm(DemoType::class,$demo);
        $demoForm->handleRequest($request);
        if($demoForm->isSubmitted() && $demoForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($demo);
            $em->flush();
            $this->addFlash('exito','Gracias por colaborar');
            
            return $this->redirectToRoute('main');

        }
        return $this->render('main/index.html.twig', [
            'controller_name' => 'Main Funcionando',
            'demoFormulario' => $demoForm->createView()
        ]);

        $rePost = $doctrine->getRepository(Posts::class);//NO FUNCIONA
        $post = $rePost->findAll();
        return $this->render('main/index.html.twig', [
            'post' => $post
        ],);

    }


}
